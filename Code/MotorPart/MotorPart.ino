volatile unsigned int temp, counter = 0;  //This variable will increase or decrease depending on the rotation of encoder
int motorRCIn;
int releaseRCIn;

typedef enum {
  STOP,
  UP,
  DOWN
} STATUS;

void setup() {
  Serial.begin(9600);

  pinMode(2, INPUT_PULLUP);  // internal pullup input pin 2

  pinMode(3, INPUT_PULLUP);  // internal pullup input pin 3
  //Setting up interrupt
  attachInterrupt(0, ai0, RISING);


  //attachInterrupt(1, ai1, RISING); //keep it commented
  pinMode(4, INPUT);  // Motor (Up, Stop and down) signal from RC
  pinMode(5, INPUT);  // Releasing signal from RC

  //Motor control pins
  pinMode(6, OUTPUT); // motor up
  pinMode(7, OUTPUT); // motor down
}

void loop() {
  // Send the value of counter
  if (counter != temp) {
    Serial.println(counter);
    temp = counter;

    // here need to add the encode function
  }

  motorRCIn   = pulseIn(4, HIGH, 25000);
  releaseRCIn = pulseIn(5, HIGH, 25000);

  if (motorRCIn > 1050 && motorRCIn < 1350)
    moveMotor(DOWN);
  else if (motorRCIn > 1750 && motorRCIn < 2000)
    moveMotor(UP);
  else
    moveMotor(STOP);

  //  if(releaseRCIn > 1400)
  //	  //here need to add the nodeMCU function
}


void ai0() {
  // ai0 is activated if DigitalPin nr 2 is going from LOW to HIGH
  // Check pin 3 to determine the direction
  if (digitalRead(3) == LOW) {
    counter++;
  } else {
    counter--;
  }
}

void ai1() {
  // ai0 is activated if DigitalPin nr 3 is going from LOW to HIGH
  // Check with pin 2 to determine the direction
  if (digitalRead(2) == LOW) {
    counter--;
  } else {
    counter++;
  }

}

void moveMotor(int status)
{
  //Maybe syntax errors
  switch (status) {
    case UP:
      analogWrite(RPWM, 255);
      analogWrite(LPWM, 0);
      break;
    case DOWN:
      analogWrite(LPWM, 255);
      analogWrite(RPWM, 0);
      break;
    case STOP:
      analogWrite(LPWM, 0);
      analogWrite(RPWM, 0);
      break;
    default:
      break;
  }
}
