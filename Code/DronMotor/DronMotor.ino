#define RPWM 5
#define LPWM 6
#define REN A1
#define LEN 4

#define MOTOR_MAX 39000 //Real
//#define MOTOR_MAX 10000

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <EEPROM.h>

const int start_counterAddress = 0;  // Address in EEPROM to store the value
int last_counterAddress = start_counterAddress + sizeof(long);
unsigned long saveMillis, saveMillis_, LEDMillis_, LEDMillis;
const unsigned long save_interval = 1000;  // Interval in milliseconds (1 second)
const unsigned long LED_interval = 200;  // Interval in milliseconds (1 second)
//We create variables for the time width values of each PWM input signal
unsigned long counter_1, counter_2, counter_3, counter_4, current_count;
long counter, start_counter;

const int buttonPin = A3;    // Button pin
const int ledPin = LED_BUILTIN;  // LED pin (for testing)

int buttonState = HIGH;       // Current state of the button
int lastButtonState = HIGH;   // Previous state of the button
unsigned long buttonPressTime = 0;  // Time when the button was pressed
bool calibMode = false;
bool resetCounter, buttonPressed, ledState;

byte last_CH1_state, last_CH2_state;

typedef enum {
  STOP,
  UP,
  DOWN
} STATUS;

RF24 radio(7, A0); // CE, CSN

const byte address[6] = "00001";

int  motorRCIn, selenoidRCIn, selenoidRCIn_;

void setup() {
  //SET PWM Interrupts
  PCICR |= (1 << PCIE0);                                                    
  PCMSK0 |= (1 << PCINT0);                                                  //D8
  PCMSK0 |= (1 << PCINT1);                                                  //D9

  //Motor Pins
  pinMode(RPWM, OUTPUT);
  pinMode(LPWM, OUTPUT);
  pinMode(LEN, OUTPUT);
  pinMode(REN, OUTPUT);
  digitalWrite(REN, HIGH);
  digitalWrite(LEN, HIGH);

  pinMode(buttonPin, INPUT_PULLUP);  // Set button pin as input with internal pull-up resistor
 // pinMode(LED_BUILTIN, OUTPUT);  // Set LED pin as output

  pinMode(2, INPUT_PULLUP);  // internal pullup input pin 3
  pinMode(3, INPUT_PULLUP);  // internal pullup input pin 3

  //Encoder
  attachInterrupt(digitalPinToInterrupt(3), ai0, RISING);

  delay(1000);
  //NRF Setup
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
  Serial.begin(9600);

  //Read current position
  EEPROM.begin();  // Initialize EEPROM

  // Read the value from EEPROM
  EEPROM.get(start_counterAddress, start_counter);
  EEPROM.get(last_counterAddress, counter);
  Serial.print("Start Position is: "); Serial.println(start_counter);
  Serial.print("Current Position is: "); Serial.println(counter);
  EEPROM.end();  // Release EEPROM
}

void loop() {
//  Serial.print("MotorRC:");    Serial.println(motorRCIn);
//  Serial.print("SelenoidRC:"); Serial.println(selenoidRCIn);
//  Serial.println(counter);
//  Serial.println("=============");

  //Serial.println(counter);

  if (!calibMode) {
    if (motorRCIn > 970 && motorRCIn < 1350) {
      if (counter > start_counter) {
        //Serial.println("UP");
        moveMotor(UP);

      }
      else {
        moveMotor(STOP);
      }
    }
    else if (motorRCIn > 1750 && motorRCIn < 2100) {
      if (counter < start_counter + MOTOR_MAX) {
        //Serial.println("DOWN");
        moveMotor(DOWN);

      }

      else {
        moveMotor(STOP);
      }

    }
    else
      moveMotor(STOP);

    char selenoid_signal;
    if (abs(selenoidRCIn_ - selenoidRCIn) > 100) {
      if (selenoidRCIn > 1400) {
        selenoid_signal = '1';
      }

      else {
        selenoid_signal = '0';
      }
      radio.write(&selenoid_signal, sizeof(selenoid_signal));
      delay(5);
      //Serial.println("Sent.");
    }
  }

  //Calibration Mode
  else {
    Calibrate();
  }

  CheckButton();

  saveMillis = millis();  // Get the current time

  // Check if the specified interval has passed
  if (saveMillis - saveMillis_ >= save_interval) {
    saveMillis_ = saveMillis;  // Update the previous time

    EEPROM.begin();  // Initialize EEPROM

    // Write the value to EEPROM
    EEPROM.put(last_counterAddress, counter);
    EEPROM.end();  // Release EEPROM
  }
}

void moveMotor(int status)
{
  //Maybe syntax errors
  switch (status) {
    case UP:
      digitalWrite(RPWM, LOW);
      digitalWrite(LPWM, HIGH);
      break;
    case DOWN:
      digitalWrite(RPWM, HIGH);
      digitalWrite(LPWM, LOW);
      break;
    case STOP:
      digitalWrite(RPWM, LOW);
      digitalWrite(LPWM, LOW);
      break;
    default:
      break;
  }
}

ISR(PCINT0_vect) {
  current_count = micros();
  //Channel 1=========================================
  if (PINB & B00000001 ) {                           //pin D8 -- B00000010
    if (last_CH1_state == 0) {
      last_CH1_state = 1;
      counter_2 = current_count;
    }
  }
  else if (last_CH1_state == 1) {
    last_CH1_state = 0;
    motorRCIn = current_count - counter_2;
  }

  if (PINB & B00000010 ) {                           //pin D9 - B00000100
    if (last_CH2_state == 0) {
      last_CH2_state = 1;
      counter_3 = current_count;
    }
  }
  else if (last_CH2_state == 1) {
    last_CH2_state = 0;
    selenoidRCIn_ = selenoidRCIn;
    selenoidRCIn = current_count - counter_3;

  }

}

void ai0() {
  // ai0 is activated if DigitalPin nr 2 is going from LOW to HIGH
  // Check pin 3 to determine the direction
  if (digitalRead(2) == LOW) {
    counter++;
  } else {
    counter--;
  }
}

void CheckButton () {
  buttonState = digitalRead(buttonPin);  // Read the button state

  // Check if button is pressed
  if (buttonState == LOW && lastButtonState == HIGH) {
    buttonPressTime = millis();  
    buttonPressed = false;  
  }

  // Check if button has been pressed for 2 seconds
  if (buttonState == LOW && (millis() - buttonPressTime) >= 2000 && !buttonPressed) {
    // Button pressed for 2 seconds
   // digitalWrite(ledPin, HIGH);  
    calibMode = !calibMode;

    if (calibMode == true) {
      counter = 0;
      Serial.print("Counter is: "); Serial.println(counter);

    }

    else {
      Serial.print("Position is: "); Serial.println(counter);
      EEPROM.begin();  // Initialize EEPROM

      // Write the value to EEPROM
      EEPROM.put(start_counterAddress, counter);
      EEPROM.end();  // Release EEPROM
    }

    buttonPressed = true;  
  }

  // Check if button is released
  if (buttonState == HIGH && lastButtonState == LOW) {
    //digitalWrite(ledPin, LOW);  // Turn off LED (for testing)
  }

  lastButtonState = buttonState;  // Update the last button state
}


void Calibrate() {

  LEDMillis = millis();  // Get the current time

  // Check if the specified interval has passed
  if (LEDMillis - LEDMillis_ >= LED_interval) {
    LEDMillis_ = LEDMillis;  // Update the previous time
    //Serial.println(ledState);
    toggleLED();  // Toggle the LED state
  }
  if (motorRCIn > 970 && motorRCIn < 1350) {
    //Serial.println("UP");
    moveMotor(UP);

  }
  else if (motorRCIn > 1750 && motorRCIn < 2100) {
    //Serial.println("DOWN");
    moveMotor(DOWN);

  }
  else
    moveMotor(STOP);

}

void toggleLED() {
  // Toggle the LED state
  ledState = !ledState;
  //digitalWrite(LED_BUILTIN, ledState);
}
