
//We create variables for the time width values of each PWM input signal
unsigned long counter_1, counter_2, counter_3, counter_4, current_count;

//We create 4 variables to stopre the previous value of the input signal (if LOW or HIGH)
byte last_CH1_state, last_CH2_state;

int D9;      
int D10;    

void setup() {
  PCICR |= (1 << PCIE0);                                                    //Set PCIE0 to enable PCMSK0 scan.
  PCMSK0 |= (1 << PCINT1);                                                  //D9
  PCMSK0 |= (1 << PCINT2);                                                  //D10
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  //Serial.print("D2: "); Serial.println(receiver_input[1]);
  Serial.print("D9: ");  Serial.println(D9);
  Serial.print("D10: "); Serial.println(D10);
  delay(500);

}

ISR(PCINT0_vect) {
  current_count = micros();
  //Channel 1=========================================
  if (PINB & B00000010 ) {                           //pin D9 -- B00000010
    if (last_CH1_state == 0) {
      last_CH1_state = 1;
      counter_2 = current_count;
    }
  }
  else if (last_CH1_state == 1) {
    last_CH1_state = 0;
    D9 = current_count - counter_2;
  }

  if (PINB & B00000100 ) {                           //pin D10 - B00000100
    if (last_CH2_state == 0) {
      last_CH2_state = 1;
      counter_3 = current_count;
    }
  }
  else if (last_CH2_state == 1) {
    last_CH2_state = 0;
    D10 = current_count - counter_3;

  }

}
