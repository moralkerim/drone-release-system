long counter;

void setup() {
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(3), ai0, RISING);

}

void loop() {
  Serial.println(counter);
  delay(500);

}

void ai0() {
  // ai0 is activated if DigitalPin nr 2 is going from LOW to HIGH
  // Check pin 3 to determine the direction
  counter++;
}
