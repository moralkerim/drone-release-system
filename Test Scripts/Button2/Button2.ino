const int buttonPin = A3;    // Button pin
const int ledPin = LED_BUILTIN;  // LED pin (for testing)

int buttonState = HIGH;       // Current state of the button
int lastButtonState = HIGH;   // Previous state of the button
unsigned long buttonPressTime = 0;  // Time when the button was pressed
bool messagePrinted = false;  // Flag to track if the message has been printed

void setup() {
  pinMode(buttonPin, INPUT_PULLUP);  // Set button pin as input with internal pull-up resistor
  pinMode(ledPin, OUTPUT);            // Set LED pin as output (for testing)
  Serial.begin(9600);  // Initialize serial communication for debugging
}

void loop() {
  buttonState = digitalRead(buttonPin);  // Read the button state

  // Check if button is pressed
  if (buttonState == LOW && lastButtonState == HIGH) {
    buttonPressTime = millis();  // Record the time the button was pressed
    messagePrinted = false;  // Reset the message printed flag
  }

  // Check if button has been pressed for 2 seconds
  if (buttonState == LOW && (millis() - buttonPressTime) >= 2000 && !messagePrinted) {
    // Button pressed for 2 seconds
    digitalWrite(ledPin, HIGH);  // Turn on LED (for testing)
    Serial.println("Button pressed for 2 seconds!");
    messagePrinted = true;  // Set the message printed flag to true
  }

  // Check if button is released
  if (buttonState == HIGH && lastButtonState == LOW) {
    digitalWrite(ledPin, LOW);  // Turn off LED (for testing)
  }

  lastButtonState = buttonState;  // Update the last button state
}
