# Drone Motor System
![IMAGE_DESCRIPTION](diagrams/Motor.jpg)
![IMAGE_DESCRIPTION](diagrams/Solenoid.jpg)
![IMAGE_DESCRIPTION](diagrams/motor_circ.jpg)
![IMAGE_DESCRIPTION](diagrams/solenoid_circ.jpg)

## Remarks
- MPPT Buck converter should be tuned to 12V and connected to the 12V Rail.
- 5V Buck converter is needed for Solenoid board.
- 24V, 5V and 3.3V converters are needed for motor board.
- Buck converter outputs can be directly connected to the voltage rails by jumpers or can be soldered seperately.
- The board enters the Calibration mode after pressing the button for 2 seconds. This can be adjusted via below section of the code.

`if (buttonState == LOW && (millis() - buttonPressTime) >= 2000 && !buttonPressed) {`

- An LED can be added to see if the board is entered to the Calibration mode or not. A Toggle LED code is already implemented in the code as belows.

`if (LEDMillis - LEDMillis_ >= LED_interval) {
    LEDMillis_ = LEDMillis;  // Update the previous time
    //Serial.println(ledState);
    toggleLED();  // Toggle the LED state
  }`

- An LED pin must be assigned to the toggle function. It can't be default Pin13 because it is being used with SPI protocol for NRF24.

`void toggleLED() {
  // Toggle the LED state
  ledState = !ledState;
  //digitalWrite(LED_BUILTIN, ledState);
}`

- Hold again for 2 seconds to exit the Calibration mode. Bring the motor to the start position and then exit. It will save its start position.

- Motor maximum turn value is calculated according to the pulley diameter. It can be changed from **MOTOR_MAX** macro.

- It saves its current encoder position each 1s. This can be adjusted by changing **save_interval** parameter.

## Debugging

### NRF24 Modules
- TX LEDs on Arduino boards should blink each time the solenoid switch on the transmitter toggles. This way it can be understood if there is a problem with transmitting part or the receiving part.
-NRF24 modules require good 3.3V sources, especially on transmitting part.

### Encoder
- Sometimes white and black wires on the encoder might get loose from the Optocoupler module. The optocopler have LEDs that show turn signals from each channel (OUTA and OUTB). Both of them should blink as the encoder rotates.





